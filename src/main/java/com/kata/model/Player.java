package com.kata.model;

import com.kata.NameGenerator;

public class Player {

    private String name;
    private Score score = Score._0;
    private int wonGames = 0;
    private int wonSets = 0;
    private boolean hasGameBall = false;

    public Player() {
        this.name = NameGenerator.get();
    }

    public Player(String name) {
        this.name = name;
    }

    public Score scoreUp() {
        if (score.index < Score._AD.index) {
            score = Score.getById(score.index + 1);
        }
        return score;
    }

    public Score getScore() {
        return score;
    }

    public void setScore(Score score) {
        this.score = score;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int gamesUp() {
        setsUp();
        return ++wonGames;
    }

    public int setsUp() {
        return ++wonSets;
    }

    public Score resetScore() {
        score = Score._0;
        hasGameBall = false;
        return score;
    }

    public void setHasGameBall(boolean hgb) {
        hasGameBall = hgb;
    }

    public int getWonGames() {
        return wonGames;
    }

    public void setWonGames(int wonGames) {
        this.wonGames = wonGames;
    }

    public int getWonSets() {
        return wonSets;
    }

    public void setWonSets(int wonSets) {
        this.wonSets = wonSets;
    }

    public boolean isHasGameBall() {
        return hasGameBall;
    }

    public boolean winsAgainst(Player opponent) {
        int scoreDiff = score.index - opponent.getScore().index;
        if (scoreDiff == 2) {
            return true;
        }
        if (wonSets - opponent.wonSets == 2) {
            return true;
        }
        return false;
    }

    public String toString() {
        return name + ": " + score.label + " points, " + "won games: " +  wonGames + ", won sets: " + wonSets ;
    }
}
