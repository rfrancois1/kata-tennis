package com.kata.model;

import java.util.HashMap;
import java.util.Map;

public enum Score {

    _0(0, "0"),
    _15(1, "15"),
    _30(2, "30"),
    _40(3, "40"),
    _AD(4, "AD");

    public int index;
    public String label;
    private static Map<Integer, Score> idToScore = new HashMap<>();
    static {
        for (Score s : Score.values()) {
            idToScore.put(s.index, s);
        }
    }

    Score(int index, String label) {
        this.index = index;
        this.label = label;
    }

    public static Score getById(int id) {
        return idToScore.get(id);
    }
}
