package com.kata;

import com.kata.model.Player;
import com.kata.model.Score;

public class Match {

    // 2 players with different names
    private Player playerOne;
    private Player playerTwo;

    public Match(String playerOneName, String playerTwoName) {
         playerOne = new Player(playerOneName);
         playerTwo = new Player(playerTwoName);
    }

    public Match() {
        String[] names = NameGenerator.getCouple();
        playerOne = new Player(names[0]);
        playerTwo = new Player(names[1]);
    }

    public String getPlayers() {
        return playerOne.getName() + " versus " + playerTwo.getName();
    }

    public String getScore() {
        return playerOne + " \\ " + playerTwo;
    }

    public void playerOneScores() {
        playerOne.scoreUp();
        if (playerOne.winsAgainst(playerTwo)) {
            playerOne.gamesUp();
            playerOne.resetScore();
            playerTwo.resetScore();
        }
    }

    public void playerTwoScores() {
        playerTwo.scoreUp();
        if (playerTwo.winsAgainst(playerOne)) {
            playerTwo.gamesUp();
            playerTwo.resetScore();
            playerOne.resetScore();
        }
    }

    public boolean hasWinner() {
        return playerOne.getWonSets() == 2 || playerTwo.getWonSets() == 2;
    }

    public boolean hasDeuce() {
        return playerOne.getScore().equals(Score._40) && playerTwo.getScore().equals(Score._40);
    }
}
