package com.kata;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public class NameGenerator {

    private static Random rand = new Random();
    private static String[] names =
            {"Raphaël", "John", "Roger", "Gaël", "Novak",
                    "Ivan", "Pete", "Andre", "Andy", "Stanislas"};

    private NameGenerator() {
    }

    public static String get() {
        return names[rand.nextInt(names.length)];
    }

    public static String[] getCouple() {
        String[] couple = {"", ""};
        ArrayList<Integer> list = new ArrayList<>();
        for (int i=0; i<names.length; i++) {
            list.add(i);
        }
        Collections.shuffle(list);
        for (int i=0; i<2; i++) {
            couple[i] = names[list.get(i)];
        }
        return couple;
    }
}
