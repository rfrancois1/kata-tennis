package com.kata.model;

import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.jupiter.api.BeforeAll;

import static org.junit.Assert.assertTrue;


public class PlayerTest {

    private Player player = new Player();

    @BeforeAll
    void init() {

    }

    @Test
    public void should_increment_score_sequentially () {
        assertTrue(player.scoreUp().equals(Score._15));
        assertTrue(player.scoreUp().equals(Score._30));
        assertTrue(player.scoreUp().equals(Score._40));
        assertTrue(player.scoreUp().equals(Score._AD));
        assertTrue(player.scoreUp().equals(Score._AD));
    }
}
