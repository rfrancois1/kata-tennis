package com.kata;

import junit.framework.TestCase;
import org.assertj.core.api.Assertions;
import org.junit.Test;


public class MatchTest {

    Match match = new Match("One", "Two");

    @Test
    public void should_display_score() {
        Assertions.assertThat(match.getScore())
                .isEqualTo("One: 0 points, won games: 0, won sets: 0 \\ Two: 0 points, won games: 0, won sets: 0");
    }

    @Test
    public void should_not_found_deuce() {
        Assertions.assertThat(match.hasDeuce()).isFalse();
    }

    @Test
    public void should_found_deuce() {
        match.playerOneScores();
        match.playerTwoScores();
        match.playerOneScores();
        match.playerTwoScores();
        match.playerOneScores();
        match.playerTwoScores();
        Assertions.assertThat(match.hasDeuce()).isTrue();
    }

    @Test
    public void should_found_winner() {

    }
}
